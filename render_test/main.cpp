//
// main.cpp
// Copyright (c) 2001 - 2011 Charles Baker.  All rights reserved.
//

#include <sweet/unit/UnitTest.h>
#include <sweet/unit/TestReporterStdout.h>

int main( int argc, char** argv )
{
    return UnitTest::RunAllTests();
}
