//
// Value.cpp
// Copyright (c) 2011 - 2012 Charles Baker.  All rights reserved.
//

#include "stdafx.hpp"
#include "Value.hpp"
#include "Renderer.hpp"
#include "Texture.hpp"
#include "Grid.hpp"
#include "Light.hpp"
#include <sweet/math/vec2.ipp>
#include <sweet/math/vec3.ipp>
#include <sweet/math/vec4.ipp>
#include <sweet/math/mat3x3.ipp>
#include <sweet/math/mat4x4.ipp>
#include <sweet/math/scalar.ipp>
#include <sweet/assert/assert.hpp>
#include <math.h>
#include <memory.h>

using std::min;
using std::max;
using std::vector;
using namespace sweet;
using namespace sweet::math;
using namespace sweet::render;

// @todo
//  Make sure that MAXIMUM_VERTICES_PER_GRID in Value.cpp is unified with the
//  same variable in Renderer.
static const int MAXIMUM_VERTICES_PER_GRID = 64 * 64;

Value::Value()
: type_( TYPE_NULL ),
  storage_( STORAGE_NULL ),
  string_value_(),
  values_( NULL ),
  size_( 0 ),
  capacity_( 0 )
{
    allocate();
}

Value::Value( const Value& value )
: type_( value.type_ ),
  storage_( value.storage_ ),
  string_value_( value.string_value_ ),
  values_( NULL ),
  size_( 0 ),
  capacity_( 0 )
{
    allocate();
    if ( value.capacity_ > 0 )
    {
        reserve( value.capacity_ );
        memcpy( values_, value.values_, capacity_ * element_size() );
        size_ = capacity_;
    }
}

Value& Value::operator=( const Value& value )
{
    if ( this != &value )
    {
        type_ = value.type_;
        storage_ = value.storage_;
        string_value_ = value.string_value_;
        size_ = value.size_;
        capacity_ = value.capacity_;
        memcpy( values_, value.values_, size_ * element_size() );
    }
    return *this;
}

Value::Value( ValueType type, ValueStorage storage )
: type_( type ),
  storage_( storage ),
  string_value_(),
  values_( NULL ),
  size_( 0 ),
  capacity_( 0 )
{
    allocate();
}

Value::Value( ValueType type, ValueStorage storage, unsigned int capacity )
: type_( type ),
  storage_( storage ),
  string_value_(),
  values_( NULL ),
  size_( 0 ),
  capacity_( 0 )
{
    allocate();
    reserve( capacity );
    size_ = capacity;
}

Value::~Value()
{
    if ( values_ )
    {
        free( values_ );
        values_ = NULL;
    }
}

Value& Value::operator=( float value )
{
    switch ( type_ )
    {
        case TYPE_FLOAT:
        {
            float* values = float_values();
            values[0] = value;
            storage_ = STORAGE_UNIFORM;
            size_ = 1;
            break;
        }
        
        case TYPE_COLOR:
        case TYPE_POINT:
        case TYPE_VECTOR:
        case TYPE_NORMAL:
        {
            vec3* values = vec3_values();
            values[0] = vec3( value, value, value );
            storage_ = STORAGE_UNIFORM;
            size_ = 1;
            break;
        }
        
        default:
            SWEET_ASSERT( false );
            break;
    }
    
    return *this;
}

Value& Value::operator=( const math::vec3& value )
{
    vec3* values = vec3_values();
    values[0] = value;        
    storage_ = STORAGE_UNIFORM;
    size_ = 1;
    return *this;
}

Value& Value::operator=( const math::mat4x4& value )
{
    mat4x4* values = mat4x4_values();
    values[0] = value;    
    storage_ = STORAGE_UNIFORM;
    size_ = 1;
    return *this;
}

Value& Value::operator=( const char* value )
{
    SWEET_ASSERT( value );
    reset( TYPE_STRING, STORAGE_UNIFORM, 1 );
    string_value_ = value;
    return *this;
}

ValueType Value::type() const
{
    return type_;
}

ValueStorage Value::storage() const
{
    return storage_;
}

void* Value::values() const
{
    return values_;
}

unsigned int Value::element_size() const
{
    unsigned int size = 0;
    switch ( type_ )
    {
        case TYPE_INTEGER:  
            size = sizeof(int);
            break;
    
        case TYPE_FLOAT:
            size = sizeof(float);
            break;
            
        case TYPE_COLOR:
        case TYPE_POINT:
        case TYPE_VECTOR:
        case TYPE_NORMAL:
            size = sizeof(math::vec3);
            break;
            
        case TYPE_MATRIX:
            size = sizeof(math::mat4x4);
            break;
            
        case TYPE_STRING:
            size = 1;
            break;
            
        default:
            SWEET_ASSERT( false );
            break;
    }
    return size;
}

void Value::set_size( unsigned int size )
{
    SWEET_ASSERT( storage_ == STORAGE_VARYING );
    size_ = size;
}

unsigned int Value::size() const
{
    return size_;
}

void Value::zero()
{
    SWEET_ASSERT( values_ );
    SWEET_ASSERT( capacity_ > 0 );
    memset( values_, 0, capacity_ * element_size() );
    size_ = capacity_;
}

void Value::clear()
{
    size_ = 0;
    capacity_ = 0;
}

void Value::reserve( unsigned int capacity )
{
    capacity_ = capacity;
    size_ = capacity;
}

void Value::reset( ValueType type, ValueStorage storage, unsigned int capacity )
{
    type_ = type;
    storage_ = storage;    
    capacity_ = capacity;
    size_ = capacity;
}

void Value::convert( ValueType type )
{
    SWEET_ASSERT( storage_ == STORAGE_CONSTANT || storage_ == STORAGE_UNIFORM );
    
    if ( type == TYPE_COLOR || type == TYPE_POINT || type == TYPE_VECTOR || type == TYPE_NORMAL )
    {
        SWEET_ASSERT( type == TYPE_FLOAT || type == TYPE_COLOR || type == TYPE_POINT || type == TYPE_VECTOR || type == TYPE_NORMAL );
        if ( type_ == TYPE_FLOAT )
        {
            float value = *float_values();
            reset( type, storage_, 1 );
            vec3* values = vec3_values();
            values[0] = vec3( value, value, value );
        }
    }
}

bool Value::empty() const
{
    return size_ == 0;
}

void Value::set_string( const std::string& value )
{
    reset( TYPE_STRING, STORAGE_UNIFORM, 1 );
    string_value_ = value;
}

const std::string& Value::string_value() const
{
    SWEET_ASSERT( type_ == TYPE_STRING );
    return string_value_;
}

const math::mat4x4& Value::mat4x4_value() const
{
    SWEET_ASSERT( storage_ == STORAGE_UNIFORM );
    return mat4x4_values()[0];
}

int* Value::int_values() const
{
    SWEET_ASSERT( type_ == TYPE_INTEGER );
    return reinterpret_cast<int*>( values_ );
}

float* Value::float_values() const
{
    SWEET_ASSERT( type_ == TYPE_FLOAT );
    return reinterpret_cast<float*>( values_ );
}

math::vec3* Value::vec3_values() const
{
    SWEET_ASSERT( type_ == TYPE_COLOR || type_ == TYPE_POINT || type_ == TYPE_VECTOR || type_ == TYPE_NORMAL );
    return reinterpret_cast<math::vec3*>( values_ );
}

math::mat4x4* Value::mat4x4_values() const
{
    SWEET_ASSERT( type_ == TYPE_MATRIX );
    return reinterpret_cast<math::mat4x4*>( values_ );
}

float Value::float_value() const
{
    SWEET_ASSERT( type_ == TYPE_FLOAT );
    SWEET_ASSERT( storage_ == STORAGE_UNIFORM );
    SWEET_ASSERT( size_ == 1 );
    return *(const float*) values_;
}

math::vec3 Value::vec3_value() const
{
    SWEET_ASSERT( type_ == TYPE_COLOR || type_ == TYPE_POINT || type_ == TYPE_VECTOR || type_ == TYPE_NORMAL );
    SWEET_ASSERT( storage_ == STORAGE_UNIFORM );
    SWEET_ASSERT( size_ == 1 );
    return *(const vec3*) values_;
}

void Value::float_to_vec3( ValueType type, ptr<Value> value )
{
    SWEET_ASSERT( value );
    SWEET_ASSERT( value->type() == TYPE_FLOAT );
    SWEET_ASSERT( type == TYPE_COLOR || type == TYPE_POINT || type == TYPE_VECTOR || type == TYPE_NORMAL );
    
    reset( type, value->storage(), value->size() );
    size_ = value->size();
    
    const int size = size_;
    vec3* values = vec3_values();
    const float* other_values = value->float_values();
    for ( int i = 0; i < size; ++i )
    {
        values[i] = vec3( other_values[i], other_values[i], other_values[i] );
    }
}

void Value::float_to_mat4x4( ptr<Value> value )
{
    SWEET_ASSERT( value );
    SWEET_ASSERT( value->type() == TYPE_FLOAT );

    reset( TYPE_MATRIX, value->storage(), value->size() );
    size_ = value->size();
    
    const int size = size_;
    mat4x4* values = mat4x4_values();
    const float* other_values = value->float_values();
    for ( int i = 0; i < size; ++i )
    {
        float x = other_values[i];
        values[i] = mat4x4( 
            x, 0.0f, 0.0f, 0.0f,
            0.0f, x, 0.0f, 0.0f,
            0.0f, 0.0f, x, 0.0f,
            0.0f, 0.0f, 0.0f, x
        );
    }
}

/**
// Calculate the light direction from the light at \e light_position (the
// explicit position expression of an illuminate statement) to the surface 
// at \e position (the implicit "Ps" value passed to a light shader).
//
// This is used to calculate "L" in a light shader's illuminate statement.
*/
void Value::light_to_surface_vector( ptr<Value> position, const math::vec3& light_position )
{
    SWEET_ASSERT( position );
    SWEET_ASSERT( position->storage() == STORAGE_VARYING );
    
    reset( TYPE_VECTOR, position->storage(), position->size() );
    size_ = position->size();
    
    const int size = size_;
    vec3* values = vec3_values();
    vec3* positions = position->vec3_values();
    for ( int i = 0; i < size; ++i )
    {
        values[i] = positions[i] - light_position;
    }
}

/**
// Calculate the light direction from the surface (the implicit "P" value in
// a surface shader) \e position to \e light.
//
// This is used to calculate "L" in a surface shader's illuminance statement 
// from the surface position and the currently active light.
*/
void Value::surface_to_light_vector( ptr<Value> position, const Light* light )
{
    SWEET_ASSERT( position );
    SWEET_ASSERT( position->storage() == STORAGE_VARYING );
    SWEET_ASSERT( light );
    
    reset( TYPE_VECTOR, position->storage(), position->size() );
    size_ = position->size();
    
    const int size = size_;
    vec3* values = vec3_values();
    const vec3* positions = position->vec3_values();
    const vec3& light_position = light->position();
    for ( int i = 0; i < size; ++i )
    {
        values[i] = light_position - positions[i];
    }
}

/**
// Calculate a mask based on the axis and angle passed to an illuminance 
// statement and the light from \e light.
*/
void Value::illuminance_axis_angle( ptr<Value> position, ptr<Value> axis, ptr<Value> angle, const Light* light )
{
    SWEET_ASSERT( position );
    SWEET_ASSERT( position->storage() == STORAGE_VARYING );
    SWEET_ASSERT( axis );
    SWEET_ASSERT( axis->storage() == STORAGE_VARYING );
    SWEET_ASSERT( position->size() == axis->size() );
    SWEET_ASSERT( angle );
    SWEET_ASSERT( light );
    
    reset( TYPE_INTEGER, position->storage(), position->size() );
    size_ = position->size();

    const int size = size_;
    const float angle_cosine = cosf( angle->float_value() );
    const vec3* axis_values = axis->vec3_values();
    const vec3* positions = position->vec3_values();
    int* values = int_values();

    switch ( light->type() )
    {
        case LIGHT_SOLAR_AXIS:
        case LIGHT_SOLAR_AXIS_ANGLE:
        {
            const vec3 light_direction = math::normalize( -light->position() );
            for ( int i = 0; i < size; ++i )
            {
                values[i] = dot( axis_values[i], light_direction ) >= angle_cosine;
            }
            break;
        }
            
        case LIGHT_ILLUMINATE:
        case LIGHT_ILLUMINATE_AXIS_ANGLE:
        {
            const vec3 light_position = light->position();
            for ( int i = 0; i < size; ++i )
            {
                values[i] = dot( axis_values[i], math::normalize(light_position - positions[i]) ) >= angle_cosine;
            }
            break;
        }
    }        
}

void Value::logical_and( ptr<Value> lhs, ptr<Value> rhs)
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->size() == rhs->size() );
    SWEET_ASSERT( lhs->type() == TYPE_INTEGER && rhs->type() == TYPE_INTEGER );    
    
    unsigned int size = lhs->size();
    reset( TYPE_INTEGER, max(lhs->storage(), rhs->storage()), size );
    
    const int* lhs_values = lhs->int_values();
    const int* rhs_values = rhs->int_values();
    int* values = int_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = int(lhs_values[i] != 0 && rhs_values[i] != 0);
    }
}

void Value::logical_or( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->size() == rhs->size() );
    SWEET_ASSERT( lhs->type() == TYPE_INTEGER && rhs->type() == TYPE_INTEGER );    
    
    unsigned int size = lhs->size();
    reset( TYPE_INTEGER, max(lhs->storage(), rhs->storage()), size );
    
    const int* lhs_values = lhs->int_values();
    const int* rhs_values = rhs->int_values();
    int* values = int_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = int(lhs_values[i] != 0 || rhs_values[i] != 0);
    }
}

void Value::transform( const math::mat4x4& m, ptr<Value> p )
{
    SWEET_ASSERT( p );
    SWEET_ASSERT( p->type() == TYPE_POINT || p->type() == TYPE_VECTOR || p->type() == TYPE_NORMAL );

    reset( p->type(), p->storage(), p->size() );

    const vec3* p_values = p->vec3_values();
    vec3* values = vec3_values();
    for ( unsigned int i = 0; i < p->size(); ++i )
    {
        values[i] = vec3( m * vec4(p_values[i], 1.0f) );
    }
}

void Value::vtransform( const math::mat4x4& m, ptr<Value> v )
{
    SWEET_ASSERT( v );
    SWEET_ASSERT( v->type() == TYPE_POINT || v->type() == TYPE_VECTOR || v->type() == TYPE_NORMAL );

    reset( v->type(), v->storage(), v->size() );

    const vec3* v_values = v->vec3_values();
    vec3* values = vec3_values();
    for ( unsigned int i = 0; i < v->size(); ++i )
    {
        values[i] = vec3( m * vec4(v_values[i], 0.0f) );
    }
}

void Value::ntransform( const math::mat4x4& mm, ptr<Value> n )
{
    SWEET_ASSERT( n );
    SWEET_ASSERT( n->type() == TYPE_POINT || n->type() == TYPE_VECTOR || n->type() == TYPE_NORMAL );

    reset( n->type(), n->storage(), n->size() );

    mat3x3 m( transpose(inverse(mat3x3(mm))) );
    const vec3* n_values = n->vec3_values();
    vec3* values = vec3_values();
    for ( unsigned int i = 0; i < n->size(); ++i )
    {
        values[i] = m * n_values[i];
    }
}

void Value::transform_matrix( const math::mat4x4& m, ptr<Value> value )
{
    SWEET_ASSERT( value );
    SWEET_ASSERT( value->type() == TYPE_MATRIX );
    
    const int size = value->size();
    reset( TYPE_MATRIX, value->storage(), value->size() );
    
    mat4x4* values = mat4x4_values();
    const mat4x4* other_values = value->mat4x4_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = m * other_values[i];
    }
}

void Value::promote_integer( int size, ptr<Value> other_value )
{
    SWEET_ASSERT( size >= 1 );
    SWEET_ASSERT( other_value );
    SWEET_ASSERT( other_value->storage() == STORAGE_UNIFORM );
    
    reset( other_value->type(), STORAGE_VARYING, size );
    size_ = capacity_;

    int value = other_value->int_values()[0];
    int* values = int_values();
    for ( int i = 0; i < size; ++i )
    {
        values[i] = value;
    }
}
 
void Value::promote_float( int size, ptr<Value> other_value )
{
    SWEET_ASSERT( size >= 1 );
    SWEET_ASSERT( other_value );
    SWEET_ASSERT( other_value->storage() == STORAGE_UNIFORM );
    
    reset( other_value->type(), STORAGE_VARYING, size );
    size_ = capacity_;

    float value = other_value->float_values()[0];
    float* values = float_values();
    for ( int i = 0; i < size; ++i )
    {
        values[i] = value;
    }
}
 
void Value::promote_vec3( int size, ptr<Value> other_value )
{           
    SWEET_ASSERT( size >= 1 );
    SWEET_ASSERT( other_value );
    SWEET_ASSERT( other_value->storage() == STORAGE_UNIFORM );
    
    reset( other_value->type(), STORAGE_VARYING, size );
    size_ = capacity_;

    vec3 value = other_value->vec3_values()[0];
    vec3* values = vec3_values();
    for ( int i = 0; i < size; ++i )
    {
        values[i] = value;
    }
}

void Value::assign_integer( ptr<Value> value, const unsigned char* mask )
{
    SWEET_ASSERT( value );

    reset( value->type(), value->storage(), value->size() );
               
    int* values = int_values();
    const int* other_values = value->int_values();
    const int size = value->size();

    if ( !mask )
    {
        for ( int i = 0; i < size; ++i )
        {
            values[i] = other_values[i];
        }
    }
    else
    {
        for ( int i = 0; i < size; ++i )
        {
            if ( mask[i] )
            {
                values[i] = other_values[i];
            }
        }
    }
}

void Value::assign_float( ptr<Value> value, const unsigned char* mask )
{
    SWEET_ASSERT( value );

    reset( value->type(), value->storage(), value->size() );
               
    float* values = float_values();
    const float* other_values = value->float_values();
    const int size = value->size();

    if ( !mask )
    {
        for ( int i = 0; i < size; ++i )
        {
            values[i] = other_values[i];
        }
    }
    else
    {
        for ( int i = 0; i < size; ++i )
        {
            if ( mask[i] )
            {
                values[i] = other_values[i];
            }
        }
    }
}

void Value::assign_vec3( ptr<Value> value, const unsigned char* mask )
{
    SWEET_ASSERT( value );
    
    reset( value->type(), value->storage(), value->size() );
               
    vec3* values = vec3_values();
    const vec3* other_values = value->vec3_values();
    const int size = value->size();

    if ( !mask )
    {
        for ( int i = 0; i < size; ++i )
        {
            values[i] = other_values[i];
        }
    }
    else
    {
        for ( int i = 0; i < size; ++i )
        {
            if ( mask[i] )
            {
                values[i] = other_values[i];
            }
        }
    }
}

void Value::assign_mat4x4( ptr<Value> value, const unsigned char* mask )
{
    SWEET_ASSERT( value );
    
    reset( value->type(), value->storage(), value->size() );
               
    mat4x4* values = mat4x4_values();
    const mat4x4* other_values = value->mat4x4_values();
    const int size = value->size();

    if ( !mask )
    {
        for ( int i = 0; i < size; ++i )
        {
            values[i] = other_values[i];
        }
    }
    else
    {
        for ( int i = 0; i < size; ++i )
        {
            if ( mask[i] )
            {
                values[i] = other_values[i];
            }
        }
    }
}

void Value::assign_string( ptr<Value> value, const unsigned char* mask )
{
    SWEET_ASSERT( value );
    SWEET_ASSERT( value->storage() != STORAGE_VARYING );
    SWEET_ASSERT( value->size() == 1 );

    reset( value->type(), value->storage(), value->size() );               
    string_value_ = value->string_value_;
}

void Value::add_assign_float( ptr<Value> value, const unsigned char* mask )
{
    SWEET_ASSERT( value );
    
    float* values = float_values();
    const float* other_values = value->float_values();
    const int size = value->size();

    if ( !mask )
    {
        for ( int i = 0; i < size; ++i )
        {
            values[i] += other_values[i];
        }
    }
    else
    {
        for ( int i = 0; i < size; ++i )
        {
            if ( mask[i] )
            {
                values[i] += other_values[i];
            }
        }
    }
}

void Value::add_assign_vec3( ptr<Value> value, const unsigned char* mask )
{
    SWEET_ASSERT( value );

    vec3* values = vec3_values();
    const vec3* other_values = value->vec3_values();
    const int size = value->size();

    if ( !mask )
    {
        for ( int i = 0; i < size; ++i )
        {
            values[i] += other_values[i];
        }
    }
    else
    {
        for ( int i = 0; i < size; ++i )
        {
            if ( mask[i] )
            {
                values[i] += other_values[i];
            }
        }
    }
}

void Value::multiply_assign_float( ptr<Value> value, const unsigned char* mask )
{
    SWEET_ASSERT( value );
    
    float* values = float_values();
    const float* other_values = value->float_values();
    const int size = value->size();

    if ( !mask )
    {
        for ( int i = 0; i < size; ++i )
        {
            values[i] *= other_values[i];
        }
    }
    else
    {
        for ( int i = 0; i < size; ++i )
        {
            if ( mask[i] )
            {
                values[i] *= other_values[i];
            }
        }
    }
}

void Value::multiply_assign_vec3( ptr<Value> value, const unsigned char* mask )
{
    SWEET_ASSERT( value );

    vec3* values = vec3_values();
    const vec3* other_values = value->vec3_values();
    const int size = value->size();

    if ( !mask )
    {
        for ( int i = 0; i < size; ++i )
        {
            values[i] *= other_values[i];
        }
    }
    else
    {
        for ( int i = 0; i < size; ++i )
        {
            if ( mask[i] )
            {
                values[i] *= other_values[i];
            }
        }
    }
}

void Value::equal_float( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->size() == rhs->size() );
    
    unsigned int size = lhs->size();
    reset( TYPE_INTEGER, max(lhs->storage(), rhs->storage()), size );
        
    const float* lhs_values = lhs->float_values();
    const float* rhs_values = rhs->float_values();
    int* values = int_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = int(lhs_values[i] == rhs_values[i]);
    }
}

void Value::equal_vec3( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->size() == rhs->size() );
    
    unsigned int size = lhs->size();
    reset( TYPE_INTEGER, max(lhs->storage(), rhs->storage()), size );
        
    const vec3* lhs_values = lhs->vec3_values();
    const vec3* rhs_values = rhs->vec3_values();
    int* values = int_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = int(lhs_values[i] == rhs_values[i]);
    }
}

void Value::not_equal_float( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->size() == rhs->size() );
    
    unsigned int size = lhs->size();
    reset( TYPE_INTEGER, max(lhs->storage(), rhs->storage()), size );
        
    const float* lhs_values = lhs->float_values();
    const float* rhs_values = rhs->float_values();
    int* values = int_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = int(lhs_values[i] != rhs_values[i]);
    }
}

void Value::not_equal_vec3( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->size() == rhs->size() );
    
    unsigned int size = lhs->size();
    reset( TYPE_INTEGER, max(lhs->storage(), rhs->storage()), size );
        
    const vec3* lhs_values = lhs->vec3_values();
    const vec3* rhs_values = rhs->vec3_values();
    int* values = int_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = int(lhs_values[i] != rhs_values[i]);
    }
}

void Value::greater_float( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( rhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( lhs->size() == rhs->size() );

    unsigned int size = lhs->size();
    reset( TYPE_INTEGER, max(lhs->storage(), rhs->storage()), size );
    
    const float* lhs_values = lhs->float_values();
    const float* rhs_values = rhs->float_values();
    int* values = int_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = int(lhs_values[i] > rhs_values[i]);
    }
}

void Value::greater_equal_float( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( rhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( lhs->size() == rhs->size() );

    unsigned int size = lhs->size();
    reset( TYPE_INTEGER, max(lhs->storage(), rhs->storage()), size );
    
    const float* lhs_values = lhs->float_values();
    const float* rhs_values = rhs->float_values();
    int* values = int_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = int(lhs_values[i] >= rhs_values[i]);
    }
}

void Value::less_float( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( rhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( lhs->size() == rhs->size() );

    unsigned int size = lhs->size();
    reset( TYPE_INTEGER, max(lhs->storage(), rhs->storage()), size );
    
    const float* lhs_values = lhs->float_values();
    const float* rhs_values = rhs->float_values();
    int* values = int_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = int(lhs_values[i] < rhs_values[i]);
    }
}

void Value::less_equal_float( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( rhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( lhs->size() == rhs->size() );

    unsigned int size = lhs->size();
    reset( TYPE_INTEGER, max(lhs->storage(), rhs->storage()), size );
    
    const float* lhs_values = lhs->float_values();
    const float* rhs_values = rhs->float_values();
    int* values = int_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = int(lhs_values[i] <= rhs_values[i]);
    }
}

void Value::inside_cone( ptr<Value> direction, const math::vec3& axis, float angle )
{
    SWEET_ASSERT( direction );
    SWEET_ASSERT( direction->type() == TYPE_VECTOR );
    SWEET_ASSERT( direction->storage() == STORAGE_VARYING );
    
    unsigned int size = direction->size();
    reset( TYPE_INTEGER, STORAGE_VARYING, size );
    size_ = size;
    
    int* inside = int_values();
    const vec3* direction_values = direction->vec3_values();
    const float cosine = cosf( angle );
    for ( unsigned int i = 0; i < size; ++i )
    {
        inside[i] = int(dot( math::normalize(direction_values[i]), axis ) >= cosine);
    }
}

void Value::negate_float( ptr<Value> value )
{
    SWEET_ASSERT( value );
    
    reset( value->type(), value->storage(), value->size() );
    const float* other_values = value->float_values();
    float* values = float_values();
    for ( unsigned int i = 0; i < value->size(); ++i )
    {
        values[i] = -other_values[i];
    }
}
            
void Value::negate_vec3( ptr<Value> value )
{
    SWEET_ASSERT( value );
    
    reset( value->type(), value->storage(), value->size() );
    const vec3* other_values = value->vec3_values();
    vec3* values = vec3_values();
    for ( unsigned int i = 0; i < value->size(); ++i )
    {
        values[i] = -other_values[i];
    }
}

void Value::add_float( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( rhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( lhs->size() == rhs->size() );

    unsigned int size = lhs->size();
    reset( TYPE_FLOAT, max(lhs->storage(), rhs->storage()), size );

    const float* lhs_values = lhs->float_values();
    const float* rhs_values = rhs->float_values();
    float* values = float_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = lhs_values[i] + rhs_values[i];
    }
}

void Value::add_vec3( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->storage() == STORAGE_VARYING );
    SWEET_ASSERT( rhs->storage() == STORAGE_VARYING );
    SWEET_ASSERT( lhs->size() == rhs->size() );

    unsigned int size = lhs->size();
    reset( TYPE_VECTOR, max(lhs->storage(), rhs->storage()), size );

    const vec3* lhs_values = lhs->vec3_values();
    const vec3* rhs_values = rhs->vec3_values();    
    vec3* values = vec3_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = lhs_values[i] + rhs_values[i];
    }
}

void Value::subtract_float( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( rhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( lhs->size() == rhs->size() );

    unsigned int size = lhs->size();
    reset( TYPE_FLOAT, max(lhs->storage(), rhs->storage()), size );

    const float* lhs_values = lhs->float_values();
    const float* rhs_values = rhs->float_values();
    float* values = float_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = lhs_values[i] - rhs_values[i];
    }
}

void Value::subtract_vec3( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->size() == rhs->size() );

    unsigned int size = lhs->size();
    reset( TYPE_VECTOR, max(lhs->storage(), rhs->storage()), size );

    const vec3* lhs_values = lhs->vec3_values();
    const vec3* rhs_values = rhs->vec3_values();    
    vec3* values = vec3_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = lhs_values[i] - rhs_values[i];
    }
}

void Value::dot_vec3( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->size() == rhs->size() );

    unsigned int size = lhs->size();
    reset( TYPE_FLOAT, max(lhs->storage(), rhs->storage()), size );

    const vec3* lhs_values = lhs->vec3_values();
    const vec3* rhs_values = rhs->vec3_values();    
    float* values = float_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = math::dot( lhs_values[i], rhs_values[i] );
    }
}

void Value::multiply_float( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( rhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( lhs->size() == rhs->size() );

    unsigned int size = lhs->size();
    reset( TYPE_FLOAT, max(lhs->storage(), rhs->storage()), size );

    const float* lhs_values = lhs->float_values();
    const float* rhs_values = rhs->float_values();
    float* values = float_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = lhs_values[i] * rhs_values[i];
    }
}

void Value::multiply_vec3( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->size() == rhs->size() );

    unsigned int size = lhs->size();
    reset( TYPE_VECTOR, max(lhs->storage(), rhs->storage()), size );

    const vec3* lhs_values = lhs->vec3_values();
    const vec3* rhs_values = rhs->vec3_values();
    vec3* values = vec3_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = lhs_values[i] * rhs_values[i];
    }
}

void Value::divide_float( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( rhs->type() == TYPE_FLOAT );
    SWEET_ASSERT( lhs->size() == rhs->size() );

    unsigned int size = lhs->size();
    reset( TYPE_FLOAT, max(lhs->storage(), rhs->storage()), size );

    const float* lhs_values = lhs->float_values();
    const float* rhs_values = rhs->float_values();
    float* values = float_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = lhs_values[i] / rhs_values[i];
    }
}

void Value::divide_vec3( ptr<Value> lhs, ptr<Value> rhs )
{
    SWEET_ASSERT( lhs );
    SWEET_ASSERT( rhs );
    SWEET_ASSERT( lhs->size() == rhs->size() );
    SWEET_ASSERT( rhs->type() == TYPE_FLOAT );

    unsigned int size = lhs->size();
    reset( lhs->type(), max(lhs->storage(), rhs->storage()), size );
    
    const vec3* lhs_values = lhs->vec3_values();
    const float* rhs_values = rhs->float_values();
    vec3* values = vec3_values();
    for ( unsigned int i = 0; i < size; ++i )
    {
        values[i] = lhs_values[i] / rhs_values[i];
    }
}

void Value::allocate()
{
    SWEET_ASSERT( !values_ );
    values_ = malloc( sizeof(vec3) * MAXIMUM_VERTICES_PER_GRID );
}
